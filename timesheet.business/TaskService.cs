﻿using System;
using System.Linq;
using timesheet.data;
using timesheet.@interface;
using timesheet.model;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace timesheet.business
{
    public class TaskService : ITaskService
    {
        public TimesheetDb Db { get; }
        public TaskService(TimesheetDb dbContext)
        {
            Db = dbContext;
        }

        public async Task<IEnumerable<model.Task>> GetTaskAsync()
        {
            return await Db.Tasks.ToListAsync();
        }        

        public async Task<int> AddTaskAsync(model.Task task)
        {
            int _response = 0;
            try
            {
                Db.Tasks.Add(task);
                _response = Db.SaveChanges();
            }
            catch (System.Exception err)
            {
                throw err;
            }

            return _response;
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
