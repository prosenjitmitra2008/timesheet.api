﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using timesheet.data;
using timesheet.helper;
using timesheet.@interface;
using timesheet.model;

namespace timesheet.business
{
    public class TaskSheetService : ITaskSheetService
    {
        public TimesheetDb Db { get; }
        public TaskSheetService(TimesheetDb dbContext)
        {
            Db = dbContext;
        }

        public async Task<IEnumerable<TaskSheet>> GetTaskSheet(int employeeId, int weekId)
        {
            try
            {

                var result = from t1 in Db.Tasks
                             join t2 in Db.TaskSheets.Where(p=>p.UserId == employeeId && p.WeekId == weekId) on t1.Id equals t2.TaskId
                             into details
                             from m in details.DefaultIfEmpty()
                             select new TaskSheet
                             {
                                 TaskId = t1.Id,
                                 RowId = string.IsNullOrEmpty(m.RowId.ToString()) ? 0 : m.RowId,
                                 UserId = Common.CheckIntegerValue(m.UserId),
                                 Sunday = Common.CheckIntegerValue(m.Sunday),
                                 Monday = Common.CheckIntegerValue(m.Monday),
                                 Tuesday = Common.CheckIntegerValue(m.Tuesday),
                                 Wednesday = Common.CheckIntegerValue(m.Wednesday),
                                 Thursday = Common.CheckIntegerValue(m.Thursday),
                                 Friday = Common.CheckIntegerValue(m.Friday),
                                 Saturday = Common.CheckIntegerValue(m.Saturday),
                             };



                return await result.ToListAsync();
            }
            catch (System.Exception err)
            {
                throw err;
            }
        }

        public async Task<int> AddTaskAsync(List<TaskSheet> taskSheet)
        {
            int _response = 0;
            try
            {
                if (taskSheet.Count > 0)
                {
                    foreach (var item in taskSheet)
                    {
                        if (item.RowId == 0)
                            Db.TaskSheets.Add(item);
                        else
                            Db.TaskSheets.Update(item);

                        _response = Db.SaveChanges();

                    }
                }
            }
            catch (System.Exception err)
            {
                throw err;
            }

            return _response;
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
