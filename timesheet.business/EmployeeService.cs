﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using timesheet.data;
using timesheet.helper;
using timesheet.@interface;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService : IEmployeeService
    {
        public TimesheetDb Db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            Db = dbContext;
        }

        public List<EmployeeVM> GetEmployeeAsync(int selectedWeek)
        {
            var _result1 = from t1 in Db.TaskSheets.Where(p=>p.WeekId == selectedWeek)
                           group t1 by t1.UserId into details
                           select new TaskVM
                           {
                               Id = (int)details.Key,
                               TotalHours = details.Sum(s => s.Sunday) + details.Sum(s => s.Monday) + details.Sum(s => s.Tuesday) + details.Sum(s => s.Wednesday) + details.Sum(s => s.Thursday) + details.Sum(s => s.Friday) + details.Sum(s => s.Saturday),
                               Average = (details.Sum(s => s.Sunday) + details.Sum(s => s.Monday) + details.Sum(s => s.Tuesday) + details.Sum(s => s.Wednesday) + details.Sum(s => s.Thursday) + details.Sum(s => s.Friday) + details.Sum(s => s.Saturday)) / 7
                           };


            var _result2 = from t1 in Db.Employees
                           join t2 in _result1 on t1.Id equals t2.Id
                           into details
                           from m in details.DefaultIfEmpty()
                           select new EmployeeVM
                           {
                               Id = (int?)t1.Id,
                               Code = t1.Code,
                               Name = t1.Name,
                               TotalHours = Common.CheckIntegerValue(m.TotalHours),
                               Average = Common.CheckDesimalValue(m.Average)
                           };

            return _result2.ToList();
        }

        public async Task<IEnumerable<Employee>> GetEmployeeFilterAsync(string text)
        {
            return await this.Db.Employees.Where(
                c => c.Code.Contains(text) ||
                c.Name.Contains(text)).ToListAsync();
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
