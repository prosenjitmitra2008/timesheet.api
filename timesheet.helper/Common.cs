﻿using System;

namespace timesheet.helper
{
    public static class Common
    {
        public static int? CheckIntegerValue(int? value)
        {
            return string.IsNullOrEmpty(value.ToString()) ? 0 : value;
        }

        public static decimal? CheckDesimalValue(decimal? average)
        {
            return string.IsNullOrEmpty(average.ToString()) ? 0.0M : average;
        }
    }
}
