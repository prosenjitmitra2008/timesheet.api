﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace timesheet.model
{
    public class Employee
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [StringLength(10)]
        [Required]
        public string Code { get; set; }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }
    }

    public class EmployeeVM
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int? TotalHours { get; set; }
        public decimal? Average { get; set; }
    }

    public class TaskVM
    {
        public int? Id { get; set; }
        public int? TotalHours { get; set; }
        public decimal? Average { get; set; }
    }
}
