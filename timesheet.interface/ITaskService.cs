﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace timesheet.@interface
{
    public interface ITaskService
    {
        Task<IEnumerable<model.Task>> GetTaskAsync();
        Task<int> AddTaskAsync(model.Task task);
    }
}
