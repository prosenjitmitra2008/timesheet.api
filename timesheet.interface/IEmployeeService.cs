﻿using System.Collections.Generic;
using System.Threading.Tasks;
using timesheet.model;

namespace timesheet.@interface
{
    public interface IEmployeeService
    {
        List<EmployeeVM> GetEmployeeAsync(int selectedWeek);
        Task<IEnumerable<Employee>> GetEmployeeFilterAsync(string text);
    }
}
