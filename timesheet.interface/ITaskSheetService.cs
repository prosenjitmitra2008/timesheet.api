﻿using System.Collections.Generic;
using System.Threading.Tasks;
using timesheet.model;

namespace timesheet.@interface
{
    public interface ITaskSheetService
    {
        Task<IEnumerable<TaskSheet>> GetTaskSheet(int employeeId, int weekId);
        Task<int> AddTaskAsync(List<TaskSheet> taskSheet);
    }
}
