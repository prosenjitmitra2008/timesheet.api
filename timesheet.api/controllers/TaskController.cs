﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.@interface;

namespace timesheet.api.controllers
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _taskService;
        public TaskController(ITaskService taskService)
        {
            this._taskService = taskService;
        }

        [HttpGet("getAll")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                IEnumerable<model.Task> response = await _taskService.GetTaskAsync();
                if (response != null)
                {
                    return new OkObjectResult(response);
                }
                else
                {
                    return new NotFoundResult();
                }
            }
            catch
            {
                return new ConflictResult();
            }
        }

        [HttpPost("addTask")]
        public async Task<IActionResult> AddTaskAsync([FromBody]model.Task task)
        {
            try
            {
                int response = await _taskService.AddTaskAsync(task);
                if (response != 0)
                {
                    return new OkObjectResult(response);
                }
                else
                {
                    return new NotFoundResult();
                }
            }
            catch
            {
                return new ConflictResult();
            }
        }

    }
}