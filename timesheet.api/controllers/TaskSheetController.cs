﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using timesheet.business;
using timesheet.@interface;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TaskSheetController : ControllerBase
    {
        private readonly ITaskSheetService _taskSheetService;
        public TaskSheetController(TaskSheetService taskSheetService)
        {
            this._taskSheetService = taskSheetService;
        }

        [HttpGet("getAll")]
        public async Task<IActionResult> GetAllAsync(int employeeId, int weekId)
        {
            try
            {
                IEnumerable<TaskSheet> response = await _taskSheetService.GetTaskSheet(employeeId,weekId);
                if (response != null)
                {
                    return new OkObjectResult(response);
                }
                else
                {
                    return new NotFoundResult();
                }
            }
            catch
            {
                return new ConflictResult();
            }
        }

        [HttpPost("addTask")]
        public async Task<IActionResult> AddTaskAsync([FromBody]List<TaskSheet> taskSheet)
        {
            try
            {
                int response = await _taskSheetService.AddTaskAsync(taskSheet);
                if (response != 0)
                {
                    return new OkObjectResult(response);
                }
                else
                {
                    return new NotFoundResult();
                }
            }
            catch
            {
                return new ConflictResult();
            }
        }

    }
}