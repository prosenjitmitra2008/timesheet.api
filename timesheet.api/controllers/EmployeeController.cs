﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.@interface;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        public EmployeeController(IEmployeeService employeeService)
        {
            this._employeeService = employeeService;
        }

        [HttpGet("getAll")]
        public async Task<IActionResult> GetAll(int selectedWeek)
        {
            try
            {
                List<EmployeeVM> response = _employeeService.GetEmployeeAsync(selectedWeek);
                if (response != null)
                {
                    return new OkObjectResult(response);
                }
                else
                {
                    return new NotFoundResult();
                }
            }
            catch (System.Exception err)
            {
                throw err;
            }            
        }

        [HttpGet("GetById")]
        public async Task<IActionResult> GetById(string text)
        {
            try
            {
                IEnumerable<Employee> response = await _employeeService.GetEmployeeFilterAsync(text);
                if (response != null)
                {
                    return new OkObjectResult(response);
                }
                else
                {
                    return new NotFoundResult();
                }
            }
            catch
            {
                return new ConflictResult();
            }
        }
    }
}